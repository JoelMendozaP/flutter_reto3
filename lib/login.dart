import 'package:flutter/material.dart';
class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurple[800],
      body: _cuerpo(),
    );
  }
  Widget _cuerpo(){
    return Container(
      //color: Colors.deepPurpleAccent,
      //height: 900,
      margin: EdgeInsets.only(top: 60),
      width: 400,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          _imagenf(),
          _contenedor()
        ],
      ),
    );
  }
  Widget _imagenf(){
    return Container(
      height: 200,
      width: 200,
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/img/corazon.png')
      ),
    )
    );
  }
  Widget _contenedor(){
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 20,right: 20),
      margin: EdgeInsets.only(right: 30),
      width: 400,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('    Welcome to', style: TextStyle(fontSize: 20)),
          _titulo(),
          SizedBox(height: 20,),
          _correo(),
          SizedBox(height: 20,),
          _password(),
          SizedBox(height: 30,),
          _boton(),
          SizedBox(height: 20,),
          Text('  Forgot Password?', style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold)),
          SizedBox(height: 20,),

        ],
      ),
    );
  }
  Widget _titulo(){
    return Text.rich(
      TextSpan(
        text: '      ',
        children: <TextSpan>[
          TextSpan(text: 'Health ', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30)),
          TextSpan(text: 'Care', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30, color: Colors.red)),
        ],
      ),
    );
  }
  Widget _correo(){
    return TextField(
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.perm_identity),
        hintText: "Enter your unsername",
        //border: OutlineInputBorder()
      ),
    );
  }
  Widget _password(){
    return TextField(
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.lock_outline),
        hintText: "Enter your password",
        //border: OutlineInputBorder()
      ),
    );
  }
  Widget _boton(){
    return Container(
      //color: Colors.yellow,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          RaisedButton(
              child: Row(children: <Widget>[
                Text("LOGIN  ",style: TextStyle(color: Colors.white)),
                Icon(Icons.arrow_forward, color: Colors.white,)
              ],),
              color: Colors.red,
              shape: StadiumBorder(),
              onPressed: (){},
          )
        ],
      )
    );
  }
}